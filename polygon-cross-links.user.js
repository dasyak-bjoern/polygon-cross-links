// ==UserScript==
// @id             iitc-plugin-polygon-cross-links@DrNoonienSoong
// @author         DrNoonienSoong
// @name           IITC plugin: polygon-cross-links
// @category       Layer
// @version        1.0.0
// @description    Checks for existing links inside polygons. Requires draw-tools and cross-links plugin.
// @include        https://www.ingress.com/intel*
// @include        http://www.ingress.com/intel*
// @match          https://www.ingress.com/intel*
// @match          http://www.ingress.com/intel*
// @require        https://secure.jonatkins.com/iitc/release/plugins/draw-tools.user.js
// @require        https://secure.jonatkins.com/iitc/release/plugins/cross_link.user.js
// @grant          none
// ==/UserScript==


function wrapper(plugin_info) {
// ensure plugin framework is there, even if iitc is not yet loaded
if(typeof window.plugin !== 'function') window.plugin = function() {};

// PLUGIN START ////////////////////////////////////////////////////////

window.plugin.polygonCrossLinks = function() {};

window.plugin.polygonCrossLinks.enabled = true;
    
/**
 * Point-in-polygon based on Jordan Curve Theorem
 * 
 * Checks whether a point with a specific lat, long coordinates is inside 
 * a polygon defined by a given set of points. You can find more information
 * about the Jordan Curve Theorem to: 
 * http://en.wikipedia.org/wiki/Jordan_curve_theorem
 * 
 * The algorithm, originally implemented in c/c++ and can be found to   
 * http://sidvind.com/wiki/Point-in-polygon:_Jordan_Curve_Theorem.
 *  
 * Implemented to Javascript by: Apostolos Kritikos <akritiko@gmail.com> 
 * Version: 1.0, July 2012
 * 
 * Parameters:
 * 	targetX - latitude (lat) of our point of interest
 * 	targetY - longtitude (long) of our point of interest 
 *
 */
window.plugin.polygonCrossLinks.pointInPolygon = function(targetX, targetY, polygon) {

	var tempX;
	var tempY;

	/* How many times the ray crosses a line-segment */
	var crossings = 0;

	/* Coordinates of the points */
	
    var polyLatLngs = polygon.getLatLngs();
	var polygonX = [];
	var polygonY = [];
    for ( var i = 0; i < polyLatLngs.length; i++) {
        polygonX[i] = polyLatLngs[i].lat;
        polygonY[i] = polyLatLngs[i].lng;
    }

	/* Iterate through each line */
	for ( var i = 0; i < polygonX.length; i++) {
		//This is done to ensure that we get the same result when the line goes from left to right and right to left
		if( polygonX[i] < polygonX[(i + 1) % polygonX.length]) {
			tempX = polygonX[i];
			tempY = polygonX[(i + 1) % polygonX.length];
		} else {
			tempX = polygonX[(i + 1) % polygonX.length];
			tempY = polygonX[i];
		}

		//First check if the ray is possible to cross the line
		if (targetX > tempX && targetX <= tempY && (targetY < polygonY[i] || targetY <= polygonY[(i + 1) % polygonX.length])) {
			var eps = 0.000001;
			//Calculate the equation of the line
			var dx = polygonX[(i + 1) % polygonX.length] - polygonX[i];
			var dy = polygonY[(i + 1) % polygonX.length] - polygonY[i];
			var k;

			if (Math.abs(dx) < eps) {
				k = 999999999999999999999999999;
			} else {
				k = dy / dx;
			}

			var m = polygonY[i] - k * polygonX[i];
			//Find if the ray crosses the line
			var y2 = k * targetX + m;
			if (targetY <= y2) {
				crossings++;
			}
		}
	}

	if (crossings % 2 == 1) {
		return true;
	} else {
		return false;
	}
};


window.plugin.polygonCrossLinks.testLinkAndPolygon = function (link,layer) {
    // for each link test if one end is inside the polygon
    var latLngs = link.getLatLngs();
    //debugger;
    for (var i = 0; i < latLngs.length; i++) {
        if (window.plugin.polygonCrossLinks.pointInPolygon(latLngs[i].lat, latLngs[i].lng, layer))
        {
            // console.debug("Link in Polygon");
            return true;
        }
            
    }

};

window.plugin.crossLinks.testLink = function (link) {
    if (plugin.crossLinks.linkLayerGuids[link.options.guid]) return;

    // console.debug($('#polygonCrossLinksEnabled').prop('checked')?"1Y":"1N");
    for (var i in plugin.drawTools.drawnItems._layers) { // leaflet don't support breaking out of the loop
       var layer = plugin.drawTools.drawnItems._layers[i];
       if (layer instanceof L.GeodesicPolygon) {
           if (plugin.crossLinks.testPolyLine(layer, link,true)) {
               plugin.crossLinks.showLink(link);
               break;
           }
           // Hier nach Links innerhalb von Polygonen suchen
           if(window.plugin.polygonCrossLinks.enabled && plugin.polygonCrossLinks.testLinkAndPolygon(link,layer)) {
               // console.debug("link in Polygon confirmed");
               plugin.crossLinks.showLink(link);
               break;
           }
        } else if (layer instanceof L.GeodesicPolyline) {
            if (plugin.crossLinks.testPolyLine(layer, link)) {
                plugin.crossLinks.showLink(link);
                break;
            }
        }
    }
};


window.plugin.crossLinks.testAllLinksAgainstLayer = function (layer) {
    if (window.plugin.crossLinks.disabled) return;

    // console.debug($('#polygonCrossLinksEnabled').prop('checked')?"2Y":"2N");
    $.each(window.links, function(guid, link) {
        if (!plugin.crossLinks.linkLayerGuids[link.options.guid])
        {
            if (layer instanceof L.GeodesicPolygon) {
                if (plugin.crossLinks.testPolyLine(layer, link,true)) {
                    plugin.crossLinks.showLink(link);
                }
                // Hier nach Links innerhalb von Polygonen suchen
                else if(window.plugin.polygonCrossLinks.enabled && plugin.polygonCrossLinks.testLinkAndPolygon(link,layer)) {
                    // console.debug("link in Polygon confirmed");
                    plugin.crossLinks.showLink(link);
                }
            } else if (layer instanceof L.GeodesicPolyline) {
                if (plugin.crossLinks.testPolyLine(layer, link)) {
                    plugin.crossLinks.showLink(link);
                }
            }
        }
    });
};
    
    window.plugin.polygonCrossLinks.setEnabled = function() {
        window.plugin.polygonCrossLinks.enabled=$('#polygonCrossLinksEnabled').prop('checked');
        // console.debug('enabled = ' + window.plugin.polygonCrossLinks.enabled);
        window.plugin.crossLinks.checkAllLinks();
    }

  window.plugin.polygonCrossLinks.dialogDrawer = function() {
    dialog({
      html:window.plugin.polygonCrossLinks.dialogLoad,
      dialogClass:'ui-dialog-polygonCrossLinks',
      title:'Cross Links Opt',
      buttons:{

      }
    });
    //window.plugin.bookmarks.autoDrawOnSelect();
  };

    window.plugin.polygonCrossLinks.dialogLoad = function() {
      r = '<div id="polygonCrossLinksDrawer">'
        + '<label style="margin-bottom: 9px; display: block;">'
        + '<input style="vertical-align: middle;" type="checkbox" id="polygonCrossLinksEnabled" checked onchange="window.plugin.polygonCrossLinks.setEnabled();return false;">'
        + ' Check Links inside polygons</label>'
        + '</div>';
    return r;
  };

  
var setup = function() {
    if (window.plugin.drawTools === undefined) {
       alert("'Polygon Cross-Links' requires 'draw-tools'");
       return;
    }
    if (window.plugin.crossLinks === undefined) {
       alert("'Polygon Cross-Links' requires 'Cross-Links'");
       return;
    }
    
    plugin.polygonCrossLinks.htmlDrawBox = '<a onclick="window.plugin.polygonCrossLinks.dialogDrawer();return false;" title="Option for cross links">Cross Links Opt</a>';
    $('#toolbox').append(window.plugin.polygonCrossLinks.htmlDrawBox);
};

// PLUGIN END //////////////////////////////////////////////////////////


setup.info = plugin_info; //add the script info data to the function as a property
if(!window.bootPlugins) window.bootPlugins = [];
window.bootPlugins.push(setup);
// if IITC has already booted, immediately run the 'setup' function
if(window.iitcLoaded && typeof setup === 'function') setup();
} // wrapper end
// inject code into site context
var script = document.createElement('script');
var info = {};
if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) info.script = { version: GM_info.script.version, name: GM_info.script.name, description: GM_info.script.description };
script.appendChild(document.createTextNode('('+ wrapper +')('+JSON.stringify(info)+');'));
(document.body || document.head || document.documentElement).appendChild(script);